package com.android.database.asset;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.android.bp.R;

public class AssetDatabaseHelper extends SQLiteOpenHelper {
	static String name="BPDB_Assets.db";
	static int dbVersion=1;
	
	private String rootDirectory = "/data/data/com.android.bp/databases/";
	private final String DATABASE_PATH = "/data/data/com.android.bp/databases/";
	private final String DATABASE_FILENAME = "BPDB_Assets.db";
	
	public AssetDatabaseHelper(Context context) {
		super(context, name, null, dbVersion);
		
		try {
			createDatabase(context);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//只在创建的时候用一次
	public void onCreate(SQLiteDatabase db) {
		//String sql="create table activitymsg(id integer primary key autoincrement,username TEXT,password varchar(20),age integer,sex varchar(2))";
		//String sql="create table goods(id integer primary key autoincrement,lineCode TEXT,enCode TEXT,goodsName TEXT,prices TEXT,count TEXT)" ;
		//db.execSQL(sql);
		
		//create table asset(id integer primary key autoincrement,planNum nvarchar(17),Number nvarchar(14),Barcode nvarchar(9),CarCode nvarchar(20),Name nvarchar(20),Spec nvarchar(20),Code nvarchar(36),Flag nvarchar(1))
        //insert into asset(planNum,Number,Barcode,CarCode,Name,Spec,Code,Flag) values('BPPD-201109000304','14030105009586','000010217','S/NL3GH838','台式电子计算机','IBM8126KCM','4dcfa719-d622-4e99-9f29-b0b38ccda030','0')
		
		//		db.beginTransaction();
//		db.execSQL("insert into goods(lineCode,enCode,goodsName,prices,count) values('40535','5000219021686','84建牌薄荷浩1毫克','18','2')");
//		db.setTransactionSuccessful();
//	    db.endTransaction();
	}
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		db.execSQL("DROP TABLE IF EXISTS " + "ST_Assets");
		onCreate(db);
	}
	


	public void createDatabase(Context context) throws IOException {
		try  
	        {   
	            // 获得.db文件的绝对路径   
	            String databaseFilename = DATABASE_PATH + DATABASE_FILENAME;   
	            File dir = new File(rootDirectory);   
	            // 如果目录不存在，创建这个目录   
	            if (!dir.exists())   
	                dir.mkdir();   
	            // 如果在/data/data/org.itec.android.Classroom
	            //目录中不存在 .db文件，则从res\raw目录中复制这个文件到该目录   
	            if (!(new File(databaseFilename)).exists()){   
	                // 获得封装.db文件的InputStream对象   
	            	 InputStream is = context.getAssets().open(DATABASE_FILENAME);
	                //InputStream is = context.getResources().openRawResource(R.raw.classroom);   
	                FileOutputStream fos = new FileOutputStream(databaseFilename);   
	                byte[] buffer = new byte[7168];   
	                int count = 0;   
	                // 开始复制.db文件   
	                while ((count = is.read(buffer)) > 0){   
	                    fos.write(buffer, 0, count);   
	                }   
	                fos.close();   
	                is.close();   
	            }   
	        }   
	        catch (Exception e){   
	        }
	}
	
	

}
