package com.android.database.asset;




import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public class AssetServices {
	private AssetDatabaseHelper dbHelper;
    private Context context;
	private static final String TABLE_NAME = "ST_Assets";// 数据表名

	public AssetServices(Context context) {
		dbHelper = new AssetDatabaseHelper(context);
        this.context = context;
	}

	

	public void closeDB() {
		// 退出程序时关闭MyDatabaseHelper里的SQLiteDatabase
		if (dbHelper != null) {
			dbHelper.close();
		}
	}
	
	
	


	/**
	 * 
	 * 删除所有数据
	 */
	public void deleteAll() {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		try {

			db.execSQL("delete from ST_Assets");

		} finally {
			db.close();
		}

	}

	
	/*//**
	 * 获取数据总数
	 * 
	 * @return
	 */
	public long getCount() {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor cursor = db.rawQuery("select count(*) from ST_Assets", null);
		cursor.moveToFirst();
		long reslut = cursor.getLong(0);
		cursor.close();
		db.close();
		return reslut;
	}
	
	
	

	
	
	/**
	 * 
	 * 检索
	 * @param Number
	 * @return
	 * @throws Exception
	 */
	public Cursor selectId(String barcode) throws Exception{
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        //
        
        String sql = "SELECT * FROM ST_Assets where Barcode=?";
        Cursor cursor = db.rawQuery(sql, new String[] { barcode});
        
        if (cursor != null && !cursor.isFirst()) {
                  cursor.moveToFirst();
        }
        
        //db.close();
        return cursor;
}
	
	
	/**
     * 更新一条数据
     * 资产盘点
     * @param user
     */
    public boolean update(int flag,String number) {
              SQLiteDatabase sdb = dbHelper.getReadableDatabase();
              
              String sql = "update ST_Assets set Flag=? where Barcode = ?";
              Object obj[] = { flag,number};

              try {
                        sdb.execSQL(sql, obj);

              } catch (SQLException e) {
                       // MyLog.i("err", "insert failed");
                        sdb.close();
                        return false;
              }

              sdb.close();
              // closeDB();
              return true;
    }
	
 // 查询某资产barcode是否存在 (根据barcode查询)
 	public boolean BarCodeExist(String barcode) throws Exception {
 		SQLiteDatabase sdb = dbHelper.getReadableDatabase();
 		String sql = "select * from ST_Assets where Barcode=?";
 		Cursor cursor = sdb.rawQuery(sql, new String[] { barcode });
 		try{
 		if ( cursor!=null && cursor.moveToFirst() == true) {
 			
 			return true;
 		}
 		}
 		finally{
 			cursor.close();
 		}
 		return false;
 	}

	
 	/**
	 * 添加一条数据
	 * 
	 * @param user
	 */
	public boolean addAsset(String num,String barcode) {
		SQLiteDatabase sdb = dbHelper.getReadableDatabase();
				
		String sql = "insert into ST_Assets(Number,Barcode) values(?,?)";
		Object obj[] = {num, barcode};

		try {
			sdb.execSQL(sql, obj);

		} catch (SQLException e) {
			//MyLog.i("err", "insert failed");
			sdb.close();
			return false;
		}

		sdb.close();
		// closeDB();
		return true;
	}
	
	/**
	 * 删除记录flag == 3
	 * 
	 * @param userid
	 */
	// TODO 删除一条数据 方法 有返回值 删除失败 返回0 成功返回 1
	public int delFlag(String flag) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		String where = " Flag= ?";
		String[] whereValue = { flag };
		int ret = db.delete(TABLE_NAME, where, whereValue);
		db.close();
		return ret;

	}
	
	
    public boolean reset() {
        SQLiteDatabase sdb = dbHelper.getReadableDatabase();//
        
        String sql = "update ST_Assets set Flag=?";
        Object obj[] = {'0'};

        try {
                  sdb.execSQL(sql, obj);

        } catch (SQLException e) {
                 // MyLog.i("err", "insert failed");
                  sdb.close();
                  return false;
        }

        sdb.close();
        // closeDB();
        return true;
}
	
    
 // TODO 查询原有资产总数
 	public  int getTotalCount() throws SQLiteException{
 		SQLiteDatabase db = dbHelper.getReadableDatabase();
 		
 		
 		String sql = "select id from ST_Assets where Flag!=3";
 		Cursor cursor = db.rawQuery(sql, null);
 		
 		int count = 0;
 		try{
 			if (cursor != null) {
 				cursor.moveToFirst();
 				count = cursor.getCount();
 			}
 		}
 		finally
 		{
 			cursor.close();
 			db.close();
 		}
 		return count;
 	}
 	
    
	// TODO 查询已盘记录数量
	public  int getInventoryCount() throws SQLiteException{
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		
		
		String sql = "select id from ST_Assets where Flag=1 or Flag=2";
		Cursor cursor = db.rawQuery(sql, null);
		
		int count = 0;
		try{
			if (cursor != null) {
				cursor.moveToFirst();
				count = cursor.getCount();
			}
		}
		finally
		{
			cursor.close();
			db.close();
		}
		return count;
	}
	
	// TODO 查询盘盈数量
	public  int getAddAssetCount() throws SQLiteException{
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		
		
		String sql = "select id from ST_Assets where Flag=3";
		Cursor cursor = db.rawQuery(sql, null);
		
		int count = 0;
		try{
			if (cursor != null) {
				cursor.moveToFirst();
				count = cursor.getCount();
			}
		}
		finally
		{
			cursor.close();
			db.close();
		}
		return count;
	}
	
	
	/**
	 * 
	 * 检索
	 * @param flag  全部资产
	 * @return
	 * @throws Exception
	 */
	public Cursor selectCursor() throws Exception{
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        //
        
        String sql = "SELECT * FROM ST_Assets where Flag !=3";
        //Cursor cursor = db.rawQuery(sql, new String[] { flag});
        Cursor cursor = db.rawQuery(sql, null);
        
        if (cursor != null && !cursor.isFirst()) {
                  cursor.moveToFirst();
        }
        
        //db.close();
        return cursor;
}
    
	//待盘数量
	public Cursor selectReadyCursor() throws Exception{
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        //
        
        String sql = "SELECT * FROM ST_Assets where Flag =0";
        //Cursor cursor = db.rawQuery(sql, new String[] { flag});
        Cursor cursor = db.rawQuery(sql, null);
        
        if (cursor != null && !cursor.isFirst()) {
                  cursor.moveToFirst();
        }
        
        //db.close();
        return cursor;
}
	
	
	// 已盘数量
	public Cursor selectAlreadyCursor() throws Exception{
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        //
        
        String sql = "SELECT * FROM ST_Assets where Flag =1 or Flag=2";
        //Cursor cursor = db.rawQuery(sql, new String[] { flag});
        Cursor cursor = db.rawQuery(sql, null);
        
        if (cursor != null && !cursor.isFirst()) {
                  cursor.moveToFirst();
        }
        
        //db.close();
        return cursor;
}
	
	// 盘盈数量
	public Cursor selectAddAssetCursor() throws Exception{
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        //
        
        String sql = "SELECT * FROM ST_Assets where Flag =3";
        //Cursor cursor = db.rawQuery(sql, new String[] { flag});
        Cursor cursor = db.rawQuery(sql, null);
        
        if (cursor != null && !cursor.isFirst()) {
                  cursor.moveToFirst();
        }
        
        //db.close();
        return cursor;
}
	
	// TODO 删除一条数据 方法 有返回值 删除失败 返回0 成功返回 1
	public int del(String barcode) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		String where = "Barcode= ?";
		String[] whereValue = { barcode };
		int ret = db.delete(TABLE_NAME, where, whereValue);
		db.close();
		return ret;

	}
	
	

}
