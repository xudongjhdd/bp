package com.android.database;

import java.io.Serializable;

public class GoodInfo implements Serializable
{

    /**
	 * 
	 */
	//Number,Barcode,ProductName,Prices,Qty,Flag
    // private static final long serialVersionUID = 1L;
    private String Number;
    private String Barcode;
    private String ProductName;
    private String Prices;
    private int Qty;
    private String Flag;

    public GoodInfo()
    {
        super();
        // TODO Auto-generated constructor stub
    }

    public GoodInfo(String Number, String Barcode, String ProductName, String Prices, int Qty,String flag)
    {
        super();
        this.Number = Number;
        this.Barcode = Barcode;
        this.ProductName = ProductName;
        this.Prices = Prices;
        this.Qty = Qty;
        this.Flag = flag;

    }

    public String getNumber()
    {
        return Number;
    }

    public void setNumber(String Number)
    {
        this.Number = Number;
    }

    public String getBarcode()
    {
        return Barcode;
    }

    public void setBarcode(String Barcode)
    {
        this.Barcode = Barcode;
    }

    public void setProductName(String ProductName)
    {
        this.ProductName = ProductName;
    }

    public String getProductName()
    {
        return ProductName;
    }

    public void setPrices(String Prices)
    {
        this.Prices = Prices;
    }

    public String getPrices()
    {
        return Prices;
    }

    public void setCount(int Qty)
    {
        this.Qty = Qty;
    }

    public int getCount()
    {
        return Qty;
    }

    
    public void setFlag(String Flag)
    {
        this.Flag = Flag;
    }

    public String getFlag()
    {
        return Flag;
    }

   
}
