package com.android.bp;

/**
 * 
 *  商品盘点主界面
 * 
 * 
 * 
 */

import com.android.database.GoodsServices;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Goods extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.goods);
	}
	
	
	
	/**
	 *
	 * @param view
	 * 
	 * 返回
	 */
	public void onBackClick(View view){  		  

        this.finish();
       
       }  
	
	
	/**
	 *
	 * @param view
	 * 
	 * 数据调整
	 */
	public void onDataSetClick(View view){  		  

		  Intent goodIntent = new Intent(this,
	                DataSet.class);
	         startActivity(goodIntent);
      
      }  
	
	/**
	 * 
	 * 商品盘点
	 * @param view
	 */
	public void onLookGoodsClick(View view){  		  

		  Intent goodIntent = new Intent(this,
	                LookGoods.class);
	         startActivity(goodIntent);
	         //this.finish();
       
       }  
	
	
	/**
	 * 
	 * 条码校验界面
	 * @param view
	 */
	public void checkSum(View view){  		  

		  Intent goodIntent = new Intent(this,
	                CheckSum.class);
	         startActivity(goodIntent);
	         //this.finish();
       
       }  
	
	
	/**
	 * 
	 * 条码校验界面
	 * @param view
	 */
	public void OnInventoryClick(View view){  		  

		  Intent goodIntent = new Intent(this,
	                Inventory.class);
	         startActivity(goodIntent);
	         //this.finish();
       
       }  
	
	
	public void deleteData(View view){  		  

	
		
		
		  AlertDialog.Builder builder = new Builder(Goods.this);

          //builder.setMessage(contents);
          builder.setTitle("确定删除所有的数据？\n" );
          //builder.setIcon(R.drawable.icon);

          builder.setPositiveButton("删除", new android.content.DialogInterface.OnClickListener()
          {
              @Override
              public void onClick(DialogInterface dialog, int which)
              {
            		GoodsServices goodsService = new GoodsServices(getApplication());
            		goodsService.reset();
             
              }
          });
          builder.setNegativeButton("取消", new android.content.DialogInterface.OnClickListener()
          {
              @Override
              public void onClick(DialogInterface dialog, int which)
              {

              }
          });
          builder.create().show();
     
     }  
	
}
