package com.android.bp;

/**
 * 
 *  商品盘点查询
 * 
 * 
 * 
 */



import java.io.File;
import java.util.HashMap;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.database.GoodInfo;
import com.android.database.GoodsServices;
import com.android.util.FileUtils;

public class Inventory extends Activity implements OnTouchListener,OnClickListener{

	
	EditText mEditText_scan;
	EditText mEditText_linecode;
	EditText mEditText_encode;
	EditText mEditText_name;
	EditText mEditText_price;
	
	
	TextView mTextView_pan_count;
	
	TextView mTextView_total_count;// 总数
	TextView mTextView_already_count;//已盘数
	TextView mTextView_rest_count;//未盘数
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.inventory);
		initView();
		initValue();
		
//		GoodsServices foodService = new GoodsServices(getApplication());
//		GoodInfo a = new GoodInfo("40535","5000219021686","84建牌薄荷浩1毫克","18","2");
//		foodService.insert(a);
		//new GoodsServices(this);
		
		
		
		 
	}
	
	
	
	/**
	 * 
	 * 初始化view
	 */
	private void initView(){
	
		mEditText_scan = (EditText) findViewById(R.id.goods_scan);
		mEditText_linecode = (EditText) findViewById(R.id.goods_linecode);
		mEditText_encode = (EditText) findViewById(R.id.goods_encode);
		mEditText_name = (EditText) findViewById(R.id.goods_name);
		mEditText_price = (EditText) findViewById(R.id.goods_price);
		
		
		mTextView_pan_count = (TextView) findViewById(R.id.goods_pan_count);
		
		mTextView_total_count = (TextView) findViewById(R.id.goods_total_count);
		mTextView_already_count = (TextView) findViewById(R.id.goods_already_count);
		mTextView_rest_count = (TextView) findViewById(R.id.goods_rest_count);
	}
	
	private void initValue(){
		
		 GoodsServices countServices = new GoodsServices(getApplication());
		 //System.out.println("已盘："+countServices.getInventoryCount());
		 //System.out.println("总数："+countServices.getProductCount());
		 mTextView_total_count.setText("总数："+countServices.getCount());
		 mTextView_already_count.setText("已盘："+countServices.getInventoryCount());
		 mTextView_rest_count.setText("未盘："+(countServices.getCount() - countServices.getInventoryCount()));
	}
	
	/**
	 *
	 * @param view
	 * 
	 * 商品盘点
	 */
	public void onOkClick(View view){  		  

		String number = mEditText_scan.getText().toString();
		//--------------------------------------
		   Cursor cursor = null;
	       
	        try
	        {
	          
	            
	            GoodsServices goodsService = new GoodsServices(getApplication());
	            cursor = goodsService.selectId(number);
	            if (cursor != null)
	            {
	               
	                    String Number = cursor.getString(cursor.getColumnIndex("Number"));
	                    String Barcode = cursor.getString(cursor.getColumnIndex("Barcode"));
	                   
	                    String ProductName = cursor.getString(cursor.getColumnIndex("ProductName"));
	                    String Prices = cursor.getString(cursor.getColumnIndex("Price"));
	                    int Qty = cursor.getInt(cursor.getColumnIndex("Qty"));
	                    String Flag = cursor.getString(cursor.getColumnIndex("Flag"));
	                    
	                    mEditText_linecode.setText(Number);
	    	            mEditText_encode.setText(Barcode);
	    	            mEditText_name.setText(ProductName);
	    	            mEditText_price.setText(Prices);
	    	            
	    	            mEditText_scan.setText("");
	    	            if(Flag.equals("1"))   mTextView_pan_count.setText("已盘数："+Qty);
	    	            
	    	          
	    	           
	            }
	            else
	            {
	    			Toast.makeText(
					getApplicationContext(),
					"数据不存在！",
					Toast.LENGTH_SHORT).show();
	            	
	            }

	            cursor.close();
	            goodsService.closeDB();
	            
	           

	        } catch (Exception e)
	        {
	            // TODO Auto-generated catch block
	        	Toast.makeText(
						getApplicationContext(),
						"数据不存在！",
						Toast.LENGTH_SHORT).show();
	            e.printStackTrace();
	        }
		//--------------------------------------
//        String linecode = mEditText_linecode.getText().toString();
//        String encode = mEditText_encode.getText().toString();
//        String name = mEditText_name.getText().toString();
//        String price = mEditText_price.getText().toString();
//        String count = mEditText_count.getText().toString();
//        
//        GoodsServices foodService = new GoodsServices(getApplication());
//        
//		GoodInfo a = new GoodInfo(linecode,encode,name,price,count);
//		try {
//			if(!foodService.goodsLineCodeExist(linecode) && !foodService.goodsEnCodeExist(encode))
//			{
//				foodService.insert(a);
//			}
//			else{
//				Toast.makeText(
//						getApplicationContext(),
//						"数据已存在",
//						Toast.LENGTH_SHORT).show();
//				
//			}
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		Toast.makeText(
//				getApplicationContext(),
//				linecode,
//				Toast.LENGTH_SHORT).show();
        
       }

	

	
	/**
	 *
	 * @param view
	 * 
	 * 商品盘点
	 */
	public void onBackClick(View view){  		  

       this.finish();
      
      }


	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		
	    Toast.makeText(this, "Button clicked!", Toast.LENGTH_SHORT).show(); 
//		if (event.getAction() == MotionEvent.ACTION_UP) {
//			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//			boolean isOpen = imm.isActive();// isOpen若返回true，则表示软键盘输入法打开
//			if (isOpen) {
//			
//				
//				imm.hideSoftInputFromWindow(
//						mEditText_scan.getWindowToken(), 0);
//				imm.hideSoftInputFromWindow(
//						mEditText_linecode.getWindowToken(), 0);
//				imm.hideSoftInputFromWindow(
//						mEditText_encode.getWindowToken(), 0);
//				
//			}
			
			

//		}
		
		return false;
	}



	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}  
	
	
}
