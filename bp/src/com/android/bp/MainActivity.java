package com.android.bp;

import com.android.bp.asset.Asset;
import com.android.database.GoodsServices;
import com.android.database.asset.AssetServices;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//new GoodsServices(this);//建立数据表 拷贝
		new AssetServices(this);
	}
	
	/**
	 *
	 * @param view
	 * 
	 * 商品盘点
	 */
	public void onGoodClick(View view){  		  
        //Toast.makeText(this, "Button clicked!", Toast.LENGTH_SHORT).show();       
        
        Intent goodIntent = new Intent(this,
                Goods.class);
         startActivity(goodIntent);
         //this.finish();
        
        }  
	
	
	
	/**
	 *
	 * @param view
	 * 
	 * 资产盘点
	 */
	public void onAssetClick(View view){  		  
       //Toast.makeText(this, "Button clicked!", Toast.LENGTH_SHORT).show();       
       
       Intent assetIntent = new Intent(this,
               Asset.class);
        startActivity(assetIntent);
        //this.finish();
       
       }  
	
	
}
