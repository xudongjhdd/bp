package com.android.bp.asset;

/**
 * 
 *  资产盘点查询
 * 
 * 
 * 
 */



import java.util.ArrayList;
import java.util.HashMap;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.bp.R;
import com.android.database.asset.AssetServices;



public class AssetInventory extends ListActivity implements OnScrollListener,OnClickListener,OnItemLongClickListener{

	TextView mTextView_asset_total_count;
	TextView mTextView_asset_ready_count;
	TextView mTextView_asset_already_count;
	TextView mTextView_asset_add_count;
	Button btn_query;//查询其资产
	EditText barcode;
	LinearLayout asset_linearlayout;
	
	 ArrayList<String>                        palnNum           = new ArrayList<String>();
	 ArrayList<String>                        Number           = new ArrayList<String>();
	 ArrayList<String>                        Barcode           = new ArrayList<String>();
	 ArrayList<String>                        Carcode           = new ArrayList<String>();
	 ArrayList<String>                        Name           = new ArrayList<String>();
	 ArrayList<String>                        Spec           = new ArrayList<String>();
	 //ArrayList<String>                        Code           = new ArrayList<String>();
	 ArrayList<String>                        Flag           = new ArrayList<String>();
	 
	 final ArrayList<HashMap<String, Object>> list                 = new ArrayList<HashMap<String, Object>>();
	 AssetAdapter                       simpleAdapter;
	 ListView                                 vncListView;
	 
	 // -------分页显示----------------------------------------------
	    int                                      scrollState;
	    int                                      count                  = 0;
	    int                                      lastItem;
	    // int visibleItemCount;
	    Button                                   footerButton;
	    LinearLayout                             footerProgressBarLayout;
	    View                                     view;

	    public static final int                  loadCountEveryTime     = 10;                                      // 每次加载条数
	    int                                      listCount              = 0;                                       // 记录已经加载个数
	    int                                      totalCount             = 0;                                       // 计算一共有多少条记录
	    int                                      arrayindex             = 0;                                        // 记录存放数组索引
	    // ------------------------------------------------------------
	    
	    int                                      index = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.asset_inventory);
		initView();
		initValue();
		initViewListener();
		
		//loadlistViewFromDB();
	}
	
	
	
	/**
	 * 
	 * 初始化view
	 */
	private void initView(){

		mTextView_asset_total_count = (TextView) findViewById(R.id.asset_total_count);
		mTextView_asset_ready_count= (TextView) findViewById(R.id.asset_ready_count);
		mTextView_asset_already_count= (TextView) findViewById(R.id.asset_already_count);
		mTextView_asset_add_count= (TextView) findViewById(R.id.asset_add_count);
		vncListView = (ListView) findViewById(android.R.id.list);
		btn_query = (Button) findViewById(R.id.asset_query);
		barcode = (EditText) findViewById(R.id.asset_barcode);
		
		asset_linearlayout = (LinearLayout) findViewById(R.id.asset_linearlayout);
		
		 // ------分页显示------------------------------------
        LayoutInflater inflater = LayoutInflater.from(this);
        view = inflater.inflate(R.layout.listview_footer_more, null);
        footerButton = (Button) view.findViewById(R.id.button);
        footerProgressBarLayout = (LinearLayout) view.findViewById(R.id.linearlayout);
        footerProgressBarLayout.setVisibility(View.GONE);
        // ---按键触发“更多”操作--------------------------------
	}
	
	
	private void initViewListener()
	{
		
		mTextView_asset_total_count.setOnClickListener(this);
		mTextView_asset_ready_count.setOnClickListener(this);
		mTextView_asset_already_count.setOnClickListener(this);
		mTextView_asset_add_count.setOnClickListener(this);
		
		vncListView.setOnItemLongClickListener(this);
		vncListView.setOnScrollListener(this);
		asset_linearlayout.setOnClickListener(this);
		 //显示更多按键
	     footerButton.setOnClickListener(new OnClickListener()
	     {

	            @Override
	            public void onClick(View v)
	            {
	            	
//	            	 Toast.makeText(AssetInventory.this, "lastItem:"+lastItem+"\n"+"listCount:"+listCount,
//	    	   		         Toast.LENGTH_SHORT).show();
	            	if (lastItem == listCount && scrollState == OnScrollListener.SCROLL_STATE_IDLE)
                    {
//	           		 Toast.makeText(AssetInventory.this, "footerButton！",
//	   		         Toast.LENGTH_SHORT).show();
	            		 footerButton.setVisibility(View.GONE);
	                    footerProgressBarLayout.setVisibility(View.GONE);
	                    
	                    AssetServices assetService = new AssetServices(getApplication());
	        		    Cursor cursor = null;
	        		   
	        		    try {
	        		    	if(index ==0)
	        				cursor = assetService.selectCursor();
	        		    	else if(index ==1)
	        		    		cursor = assetService.selectReadyCursor();
	        			} catch (Exception e) {
	        				// TODO Auto-generated catch block
	        				e.printStackTrace();
	        			}
	                    getAssetFromDB(list.size(),cursor);
	                    simpleAdapter.notifyDataSetChanged();// 更新UI
	                    //loadlistViewFromDB();
	                    footerButton.setVisibility(View.VISIBLE);
                        footerProgressBarLayout.setVisibility(View.GONE);
                    }
	            	
	            }

	     });
		
	}
	
	
	
	
	
	
	private void initValue(){
	
		AssetServices assetServices = new AssetServices(getApplication());
		
		mTextView_asset_total_count.setText(Html.fromHtml("<u>"+"资产总数："+assetServices.getTotalCount()+"</u>"));
		mTextView_asset_already_count.setText(Html.fromHtml("<u>"+"已盘数量："+assetServices.getInventoryCount()+"</u>"));
		mTextView_asset_ready_count.setText(Html.fromHtml("<u>"+"待盘数量："+(assetServices.getTotalCount()-assetServices.getInventoryCount())+"</u>"));
		mTextView_asset_add_count.setText(Html.fromHtml("<u>"+"盘盈数量："+assetServices.getAddAssetCount()+"</u>"));
		//totalCount = assetServices.getTotalCount();
	    //System.out.println("资产总数："+assetServices.getCount());
	    //System.out.println("已盘数："+assetServices.getInventoryCount());
	    //System.out.println("盘盈数："+assetServices.getAddAssetCount());
	}
	
	
	 /**
     * 
     * 从本地数据中中加载数据
     */
    public void loadlistViewFromDB(Cursor cursor)
    {
    	//Cursor cursor
    	getAssetFromDB(list.size(),cursor);
        simpleAdapter = buildSimpleAdapter();
        vncListView.setAdapter(simpleAdapter);
        
    }
	
	
	/**
	 * 
	 * 获取全部资产列表
	 */
	private void getAssetFromDB(int formIndex,Cursor cursor)
	{
		//AssetServices assetService = new AssetServices(getApplication());
	    //Cursor cursor = null;
	    try {
	    	//cursor = assetService.selectCursor();
	    	cursor.moveToFirst();
	    	cursor.moveToPosition(formIndex);
	        if (cursor != null)
            {
	        	listCount = listCount + loadCountEveryTime;
                //for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext())
	        	for (arrayindex = 0; arrayindex < loadCountEveryTime; cursor.moveToNext(),arrayindex++)
                {
                    String plannum = cursor.getString(cursor.getColumnIndex("PlanNum"));
	        		
                    String number = cursor.getString(cursor.getColumnIndex("Number"));
                    String barcode = cursor.getString(cursor.getColumnIndex("Barcode"));
                    String carcode = cursor.getString(cursor.getColumnIndex("CarCode"));
                    String name = cursor.getString(cursor.getColumnIndex("Name"));
                    String spec = cursor.getString(cursor.getColumnIndex("Spec"));
                    String flag = cursor.getString(cursor.getColumnIndex("Flag"));
                    
//                    System.out.println("#####plannum:"+plannum);
//                    System.out.println("#####number:"+number);
//                    System.out.println("#####barcode:"+barcode);
//                    System.out.println("#####carcode:"+carcode);
//                    System.out.println("#####name:"+name);
//                    System.out.println("#####spec:"+spec);
                    
                    
                    palnNum.add(plannum);
                    Number.add(number);
                    Barcode.add(barcode);
                    Carcode.add(carcode);
                    Name.add(name);
                    Spec.add(spec);
                    Flag.add(flag);
                    
                    HashMap<String, Object> map = new HashMap<String, Object>();

                    map.put("planNum", plannum);
                    map.put("Number", number);
                    map.put("Barcode", barcode);
                    map.put("CarCode", carcode);
                    map.put("Name", name);
                    map.put("Spec", spec); 
                    map.put("Flag", flag); 
                    list.add(map);

                }

           

            }

            cursor.close();
            //assetService.closeDB();
	    	
	    	
	    	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
	}
	
	private void clearArray()
	{
		  palnNum.clear();;
          Number.clear();
          Barcode.clear();
          Carcode.clear();
          Name.clear();
          Spec.clear();
          list.clear();
          Flag.clear();
          
         count  = 0;
  	     lastItem=0;
  	             
  	     listCount  = 0;                                       // 记录已经加载个数
//  	     totalCount = 0;                                 // 计算一共有多少条记录
  	     arrayindex = 0;   
  	   vncListView.removeFooterView(view);
	}
	/**
	 * 
	 * 获取未盘资产列表
	 */
	private void getReadyAssetFromDB()
	{
		AssetServices assetService = new AssetServices(getApplication());
	    Cursor cursor = null;
	    try {
	    	cursor = assetService.selectReadyCursor();
	    	
	        if (cursor != null)
            {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext())
                {
                    String plannum = cursor.getString(cursor.getColumnIndex("PlanNum"));
                    String number = cursor.getString(cursor.getColumnIndex("Number"));
                    String barcode = cursor.getString(cursor.getColumnIndex("Barcode"));
                    String carcode = cursor.getString(cursor.getColumnIndex("CarCode"));
                    String name = cursor.getString(cursor.getColumnIndex("Name"));
                    String spec = cursor.getString(cursor.getColumnIndex("Spec"));
                    String flag = cursor.getString(cursor.getColumnIndex("Flag"));
                    
//                    System.out.println("#####plannum:"+plannum);
//                    System.out.println("#####number:"+number);
//                    System.out.println("#####barcode:"+barcode);
//                    System.out.println("#####carcode:"+carcode);
//                    System.out.println("#####name:"+name);
//                    System.out.println("#####spec:"+spec);
                    
                    
                    palnNum.add(plannum);
                    Number.add(number);
                    Barcode.add(barcode);
                    Carcode.add(carcode);
                    Name.add(name);
                    Spec.add(spec);
                    Flag.add(flag);
                    HashMap<String, Object> map = new HashMap<String, Object>();

                    map.put("planNum", plannum);
                    map.put("Number", number);
                    map.put("Barcode", barcode);
                    map.put("CarCode", carcode);
                    map.put("Name", name);
                    map.put("Spec", spec);   
                    map.put("Flag", flag); 
                    list.add(map);

                }

           

            }

            cursor.close();
            assetService.closeDB();
	    	
	    	
	    	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
	}
	
	
	/**
	 * 
	 * 获取已盘资产列表
	 */
	private void getAlReadyAssetFromDB()
	{
		AssetServices assetService = new AssetServices(getApplication());
	    Cursor cursor = null;
	    try {
	    	cursor = assetService.selectAlreadyCursor();
	    	
	        if (cursor != null)
            {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext())
                {
                    String plannum = cursor.getString(cursor.getColumnIndex("PlanNum"));
                    String number = cursor.getString(cursor.getColumnIndex("Number"));
                    String barcode = cursor.getString(cursor.getColumnIndex("Barcode"));
                    String carcode = cursor.getString(cursor.getColumnIndex("CarCode"));
                    String name = cursor.getString(cursor.getColumnIndex("Name"));
                    String spec = cursor.getString(cursor.getColumnIndex("Spec"));
                    String flag = cursor.getString(cursor.getColumnIndex("Flag"));
                    
                    
                    palnNum.add(plannum);
                    Number.add(number);
                    Barcode.add(barcode);
                    Carcode.add(carcode);
                    Name.add(name);
                    Spec.add(spec);
                    Flag.add(flag);
                    HashMap<String, Object> map = new HashMap<String, Object>();

                    map.put("planNum", plannum);
                    map.put("Number", number);
                    map.put("Barcode", barcode);
                    map.put("CarCode", carcode);
                    map.put("Name", name);
                    map.put("Spec", spec);   
                    map.put("Flag", flag);   
                    list.add(map);

                }

           

            }

            cursor.close();
            assetService.closeDB();
	    	
	    	
	    	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
	}
	
	
	
	/**
	 * 
	 * 获取盘盈资产列表
	 */
	private void getAddAssetFromDB()
	{
		AssetServices assetService = new AssetServices(getApplication());
	    Cursor cursor = null;
	    try {
	    	cursor = assetService.selectAddAssetCursor();
	    	
	        if (cursor != null)
            {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext())
                {
                    String plannum = cursor.getString(cursor.getColumnIndex("PlanNum"));
                    String number = cursor.getString(cursor.getColumnIndex("Number"));
                    String barcode = cursor.getString(cursor.getColumnIndex("Barcode"));
                    String carcode = cursor.getString(cursor.getColumnIndex("CarCode"));
                    String name = cursor.getString(cursor.getColumnIndex("Name"));
                    String spec = cursor.getString(cursor.getColumnIndex("Spec"));
                    String flag = cursor.getString(cursor.getColumnIndex("Flag"));
                    
                    
                    palnNum.add(plannum);
                    Number.add(number);
                    Barcode.add(barcode);
                    Carcode.add(carcode);
                    Name.add(name);
                    Spec.add(spec);
                    Flag.add(flag);
                    HashMap<String, Object> map = new HashMap<String, Object>();

                    map.put("planNum", plannum);
                    map.put("Number", number);
                    map.put("Barcode", barcode);
                    map.put("CarCode", carcode);
                    map.put("Name", name);
                    map.put("Spec", spec);      
                    map.put("Flag", flag);      
                    list.add(map);

                }

           

            }

            cursor.close();
            assetService.closeDB();
	    	
	    	
	    	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
	}
	
	
	
	
	/**
	 * 
	 * 获取特定资产列表
	 */
	private void getAssetFromDB(String code)
	{
		AssetServices assetService = new AssetServices(getApplication());
	    Cursor cursor = null;
	    try {
	    	
	    	cursor = assetService.selectId(code);
	        if (cursor != null)
            {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext())
                {
                    String plannum = cursor.getString(cursor.getColumnIndex("PlanNum"));
                    String number = cursor.getString(cursor.getColumnIndex("Number"));
                    String barcode = cursor.getString(cursor.getColumnIndex("Barcode"));
                    String carcode = cursor.getString(cursor.getColumnIndex("CarCode"));
                    String name = cursor.getString(cursor.getColumnIndex("Name"));
                    String spec = cursor.getString(cursor.getColumnIndex("Spec"));
                    String flag = cursor.getString(cursor.getColumnIndex("Flag"));
                    
                    
                    palnNum.add(plannum);
                    Number.add(number);
                    Barcode.add(barcode);
                    Carcode.add(carcode);
                    Name.add(name);
                    Spec.add(spec);
                    Flag.add(flag);
                    HashMap<String, Object> map = new HashMap<String, Object>();

                    map.put("planNum", plannum);
                    map.put("Number", number);
                    map.put("Barcode", barcode);
                    map.put("CarCode", carcode);
                    map.put("Name", name);
                    map.put("Spec", spec);   
                    map.put("Flag", flag);
                    list.add(map);

                }

           

            }

            cursor.close();
            assetService.closeDB();
	    	
	    	
	    	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
	}
	
	
	//-------------------------
	 private AssetAdapter buildSimpleAdapter()
	    {

	        // 生成适配器的Item和动态数组对应的元素
		 AssetAdapter listItemAdapter = new AssetAdapter(this, list,// 数据源
	                R.layout.asset_inventory_item,// ListItem的XML实现

	                // 动态数组与ImageItem对应的子项
	                new String[] { "planNum", "Number", "Barcode", "CarCode", "Name","Spec","Flag" },

	                new int[] { R.id.asset_plan, R.id.asset_number, R.id.asset_barcode, R.id.asset_carcode, R.id.asset_name, R.id.asset_spec,R.id.asset_status }

	        );

	        // ----------------------
	        // 将这个SimpleAdapter对象设置到ListActivity当中
	        return listItemAdapter;
	    }
	
	//----------------------------------------
	 public class AssetAdapter extends BaseAdapter
	 {
	        private class buttonViewHolder
	        {

	          
	            TextView       Plannum;                    //
	            TextView       Number;                   //
	            TextView       Barcode;                
	            TextView       Carcode;             // 是否购买选择
	            TextView       Name;
	            TextView       Spec;
	            TextView       Flag;
	        }

	        private ArrayList<HashMap<String, Object>> mAppList;
	        private LayoutInflater                     mInflater;
	        private Context                            mContext;
	        private String[]                           keyString;
	        private int[]                              valueViewID;
	        private buttonViewHolder                   holder;
	        

	        public AssetAdapter(Context c, ArrayList<HashMap<String, Object>> list, int resource, String[] from, int[] to)
	        {
	            mAppList = list;
	            mContext = c;
	            mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            keyString = new String[from.length];
	            valueViewID = new int[to.length];
	            System.arraycopy(from, 0, keyString, 0, from.length);
	            System.arraycopy(to, 0, valueViewID, 0, to.length);
	            

	        }

	        @Override
	        public int getCount()
	        {
	            return mAppList.size();
	        }

	        @Override
	        public Object getItem(int position)
	        {
	            return mAppList.get(position);
	        }

	        @Override
	        public long getItemId(int position)
	        {
	            return position;
	        }

	        @Override
	        public View getView(int position, View convertView, ViewGroup parent)
	        {

	           
	            // View rowView = convertView;
	            HashMap<String, Object> appInfo = null;
	            if (convertView != null)
	            {
	                holder = (buttonViewHolder) convertView.getTag();
	                // System.out.println("convertView != null");

	                // convertView.setTag(holder);
	            } else
	            {

	                convertView = mInflater.inflate(R.layout.asset_inventory_item, null);

	                holder = new buttonViewHolder();
	                holder.Plannum = (TextView) convertView.findViewById(valueViewID[0]);

	                holder.Number = (TextView) convertView.findViewById(valueViewID[1]);

	                holder.Barcode = (TextView) convertView.findViewById(valueViewID[2]);

	                holder.Carcode = (TextView) convertView.findViewById(valueViewID[3]);

	                holder.Name = (TextView) convertView.findViewById(valueViewID[4]);
	              
	                holder.Spec = (TextView) convertView.findViewById(valueViewID[5]);
	                
	                holder.Flag = (TextView) convertView.findViewById(valueViewID[6]);
	                convertView.setTag(holder);

	            }

	            appInfo = mAppList.get(position);

	            if (appInfo != null)
	            {

	                String planNum = (String) appInfo.get(keyString[0]);//
	                String Number = (String) appInfo.get(keyString[1]);//
	                String Barcode = (String) appInfo.get(keyString[2]);//
	                String Carcode = (String) appInfo.get(keyString[3]);//
	                String Name = (String) appInfo.get(keyString[4]);//
	                String Spec = (String) appInfo.get(keyString[5]);//
	                String Flag = (String) appInfo.get(keyString[6]);
	                // -----------------------------------------------------------------

	                // simpleAdapter.notifyDataSetChanged();// 更新UI
	                // ----------------------------------

	                holder.Plannum.setText("盘点计划："+planNum);
	                holder.Number.setText("资产编号："+Number);
	                holder.Barcode.setText("资产条码："+Barcode);
	                holder.Carcode.setText("车牌井："+Carcode);
	                holder.Name.setText("名称："+Name);
	                holder.Spec.setText("规格："+Spec);
	                String status;
	                if(Flag.equals("1")) status = "在用";
	                else if(Flag.equals("2")) status = "闲置";
	                else if(Flag.equals("3")) status = "盘盈";
	                else status = "";
	                
	                holder.Flag.setText("状态："+Flag+status);
	            }

	            return convertView;
	        }

	      
	       
	       

	    }
	
	//-----------------------------------------
	
	
	
	
	/**
	 *
	 * @param view
	 * 
	 * 资产查询
	 */
	public void onOkClick(View view){  		  
		String code = barcode.getText().toString();
		vncListView.setLongClickable(false);
		clearArray();
		getAssetFromDB(code);
        simpleAdapter = buildSimpleAdapter();// web service // 下载数据
        vncListView.setAdapter(simpleAdapter);
       }

	

	
	/**
	 *
	 * @param view
	 * 
	 * 商品盘点
	 */
	public void onBackClick(View view){  		  

       this.finish();
      
      }






	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		
//		    Toast.makeText(this, "onListItemClick！"+position,
//         Toast.LENGTH_SHORT).show();
	}


	
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()){
		case R.id.asset_total_count:
			index= 0;
			vncListView.setLongClickable(false);
			// ----添加到脚页显示-----

			clearArray();
			
			vncListView.addFooterView(view);
	        
	        
	       // vncListView.setOnScrollListener(this);
	        // --------------
			
			
			AssetServices assetService = new AssetServices(getApplication());
		    Cursor cursor = null;
		    totalCount = assetService.getTotalCount();
		    try {
				cursor = assetService.selectCursor();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			loadlistViewFromDB(cursor);
		break;
		case R.id.asset_ready_count:
			index= 1;
			vncListView.setLongClickable(false);
//			clearArray();
//			getReadyAssetFromDB();
//	        simpleAdapter = buildSimpleAdapter();// web service // 下载数据
//	        vncListView.setAdapter(simpleAdapter);
			
			clearArray();
			vncListView.addFooterView(view);				
			AssetServices assetReadyService = new AssetServices(getApplication());
		    Cursor readyCursor = null;
		    totalCount = assetReadyService.getTotalCount()-assetReadyService.getInventoryCount();
		    try {
		    	readyCursor = assetReadyService.selectReadyCursor();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			loadlistViewFromDB(readyCursor);
			
			
			break;
		case R.id.asset_already_count:
			vncListView.setLongClickable(false);
			clearArray();
			getAlReadyAssetFromDB();
	        simpleAdapter = buildSimpleAdapter();// web service // 下载数据
	        vncListView.setAdapter(simpleAdapter);
			break;
		case R.id.asset_add_count:
			vncListView.setLongClickable(true);
			clearArray();
			getAddAssetFromDB();
	        simpleAdapter = buildSimpleAdapter();// web service // 下载数据
	        vncListView.setAdapter(simpleAdapter);
			break;
		case R.id.asset_linearlayout:
			

			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			boolean isOpen = imm.isActive();// isOpen若返回true，则表示软键盘输入法打开
			if (isOpen) {
				
				imm.hideSoftInputFromWindow(
						barcode.getWindowToken(), 0);
				
			}
			
			

		
			break;
			
		}
	}



	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			final int position, long id) {
		// TODO Auto-generated method stub
		
//		 Toast.makeText(this, "onListItemLongClick！"+position,
//		         Toast.LENGTH_SHORT).show();
		 

         
	        // Toast.makeText(activity.this, "onItemLongClick！" + arg2,
	        // Toast.LENGTH_SHORT).show();
	        // ----------------------------------
	        final String[] itemsa = { "删除记录"/*, "查看所有回复"/* , "查看资料" */};
	        new AlertDialog.Builder(this).setTitle("选项")

	        // setSingleChoiceItems()的第二个参数是设置默认选项，选项索引从0开始，-1代表不选择任何选项。
	                .setItems(itemsa, new DialogInterface.OnClickListener()
	                {
	                    public void onClick(DialogInterface dialog, int item)
	                    {
	                    	if(item == 0)
	                    	{
	                    		 Toast.makeText(AssetInventory.this, ""+Barcode.get(position),
	                	         Toast.LENGTH_SHORT).show();
	                    		 AssetServices assetService = new AssetServices(getApplication());
	                    		int ok = assetService.del(Barcode.get(position));
	                    		if(ok == 1)//删除成功
	                    		{
	                    			mTextView_asset_add_count.setText(Html.fromHtml("<u>"+"盘盈数量："+assetService.getAddAssetCount()+"</u>"));
	                    		
	                    			vncListView.setLongClickable(true);
	                    			clearArray();
	                    			getAddAssetFromDB();
	                    	        simpleAdapter = buildSimpleAdapter();// web service // 下载数据
	                    	        vncListView.setAdapter(simpleAdapter);
	                    		}
	                    	}
	                    }
	                }).show();
	        // }).setPositiveButton("确定", null).show();

	        // //------------------------------------
	        return true;
	    
		 
		 
		//return false;
	}



	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub
		   this.scrollState = scrollState;
	}



	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub
//	    Toast.makeText(this, "onScroll！"+visibleItemCount,
//      Toast.LENGTH_SHORT).show();

        lastItem = firstVisibleItem + visibleItemCount - 1;

        System.out.println("listCount:"+listCount);
        System.out.println("totalCount:"+totalCount);
        System.out.println("lastItem:"+lastItem);
        
        if (listCount > totalCount && lastItem == totalCount)
        {

            // System.out.println("removeFooterView");
            vncListView.removeFooterView(view);
            footerButton.setVisibility(View.GONE);
        }else
        {
        	//vncListView.addFooterView(view);
        	footerButton.setVisibility(View.VISIBLE);
        }

	}



	
	
}
