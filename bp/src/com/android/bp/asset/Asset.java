package com.android.bp.asset;

/**
 * 
 *  商品盘点主界面
 * 
 * 
 * 
 */

import com.android.bp.R;
import com.android.database.GoodsServices;
import com.android.database.asset.AssetServices;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Asset extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.asset);
	}
	
	
	
	/**
	 *
	 * @param view
	 * 
	 * 返回
	 */
	public void onBackClick(View view){  		  

        this.finish();
       
       }  
	
	
	/**
	 *
	 * @param view
	 * 
	 * 数据调整
	 */
	public void onDataSetClick(View view){  		  

//		  Intent goodIntent = new Intent(this,
//	                DataSet.class);
//	         startActivity(goodIntent);
      
      }  
	
	/**
	 * 
	 * 资产盘点
	 * @param view
	 */
	public void onLookAssetClick(View view){  		  

		  Intent goodIntent = new Intent(this,
	                LookAsset.class);
	         startActivity(goodIntent);
	         //this.finish();
       
       }  
	

	
	
	/**
	 * 
	 * 资产查询
	 * @param view
	 */
	public void OnAssetInventoryClick(View view){  		  

		  Intent assetIntent = new Intent(this,
	                AssetInventory.class);
	         startActivity(assetIntent);
	         //this.finish();
       
       }  
	
	
	public void deleteData(View view){  		  

	
		
		
		  AlertDialog.Builder builder = new Builder(Asset.this);

          //builder.setMessage(contents);
          builder.setTitle("确定删除所有的数据？\n" );
          //builder.setIcon(R.drawable.icon);

          builder.setPositiveButton("删除", new android.content.DialogInterface.OnClickListener()
          {
              @Override
              public void onClick(DialogInterface dialog, int which)
              {
            	  AssetServices assetService = new AssetServices(getApplication());
            	  assetService.delFlag("3");
            	  assetService.reset();
             
              }
          });
          builder.setNegativeButton("取消", new android.content.DialogInterface.OnClickListener()
          {
              @Override
              public void onClick(DialogInterface dialog, int which)
              {

              }
          });
          builder.create().show();
     
     }  
	
}
