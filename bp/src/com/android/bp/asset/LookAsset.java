package com.android.bp.asset;

/**
 * 
 *  商品盘点子界面
 * 
 * 
 * 
 */



import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.bp.R;
import com.android.database.asset.AssetServices;

public class LookAsset extends Activity implements OnTouchListener,OnClickListener{

	
	EditText mEditText_asset_barcode_scan;
	EditText mEditText_asset_number;
	EditText mEditText_asset_barcode;
	EditText mEditText_asset_name;
	EditText mEditText_asset_playnum;
	EditText mEditText_asset_carcode;
	EditText mEditText_asset_spec;
	
	Button assetSave;
	
	TextView mTextView_pan_count;
   
	    private static final String[] m={"","1在用","2闲置"};
	    TextView spinnerView ;
	    private Spinner spinner;
	    private ArrayAdapter<String> adapter;
	
	    int spinnerNum;
	    
	    boolean bool_assetAdd=false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lookasset);
		initView();
		//initValue();
		initSpinnerView();
		
//		GoodsServices foodService = new GoodsServices(getApplication());
//		GoodInfo a = new GoodInfo("40535","5000219021686","84建牌薄荷浩1毫克","18","2");
//		foodService.insert(a);
		//new GoodsServices(this);
	}
	
	private void initSpinnerView(){
		spinnerView = (TextView) findViewById(R.id.spinnerText);
        spinner = (Spinner) findViewById(R.id.Spinner_status);
        //将可选内容与ArrayAdapter连接起来
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,m);
         
        //设置下拉列表的风格
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
         
        //将adapter 添加到spinner中
        spinner.setAdapter(adapter);
         
        //添加事件Spinner事件监听  
        spinner.setOnItemSelectedListener(new SpinnerSelectedListener());
         
        //设置默认值
        spinner.setVisibility(View.VISIBLE);
        
        //spinner.setSelection(2,true);
		
	}
	
	//使用数组形式操作
    class SpinnerSelectedListener implements OnItemSelectedListener{
 
    

		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			// TODO Auto-generated method stub
			//view.setText("你的血型是："+m[id]);
//			 Toast.makeText(
//						getApplicationContext(),
//						m[position],
//						Toast.LENGTH_SHORT).show();
			
			spinnerNum = position;
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
			// TODO Auto-generated method stub
			
		}
    }
    
	
	/**
	 * 
	 * 初始化view
	 */
	private void initView(){
			
		
		mEditText_asset_barcode_scan = (EditText) findViewById(R.id.asset_barcode_scan);
		mEditText_asset_number = (EditText) findViewById(R.id.asset_number);
		mEditText_asset_barcode = (EditText) findViewById(R.id.asset_barcode);
		mEditText_asset_name = (EditText) findViewById(R.id.asset_name);
		mEditText_asset_playnum = (EditText) findViewById(R.id.asset_playnum);
		mEditText_asset_carcode = (EditText) findViewById(R.id.asset_carcode);		
		mEditText_asset_spec = (EditText) findViewById(R.id.asset_spec);
		
		assetSave = (Button) findViewById(R.id.asset_save);
	}
	
	
	private void initValue() {}
	
	
	
	/**
	 *
	 * @param view
	 * 
	 * 资产盘点
	 */
	public void onOkClick(View view){  		  

		mEditText_asset_number.setText("");
		String number = mEditText_asset_barcode_scan.getText().toString();
		//--------------------------------------
		   Cursor cursor = null;
	       
	        try
	        {
	          
	            //System.out.println("########"+number);
	        	AssetServices assetServices = new AssetServices(getApplication());
	        	if(!assetServices.BarCodeExist(number))
	        	{
//	        		Toast.makeText(
//	    					getApplicationContext(),
//	    					"数据不存在！是否要存入",
//	    					Toast.LENGTH_SHORT).show();
	        		assetAdd();
	        		return;
	        	}
	        	cursor = assetServices.selectId(number);
	            if (cursor != null)
	            {
	                    //盘点计划
	                    String playnum = cursor.getString(cursor.getColumnIndex("PlanNum"));
	                    //资产编号
	                    String Number = cursor.getString(cursor.getColumnIndex("Number"));
	                    //条形码
	                    String Barcode = cursor.getString(cursor.getColumnIndex("Barcode"));
	                    //车牌井号
	                    String Carcode = cursor.getString(cursor.getColumnIndex("CarCode"));
	                    //资产名称
	                    String name = cursor.getString(cursor.getColumnIndex("Name"));
	                    //规格型号
	                    String spec = cursor.getString(cursor.getColumnIndex("Spec"));
	                    String flag = cursor.getString(cursor.getColumnIndex("Flag"));
	                    
	                    
	                    
	                    if(TextUtils.isEmpty(playnum) && !TextUtils.isEmpty(Number))
	                    {
	                    	 AlertDialog.Builder builder = new Builder(LookAsset.this);

	                         //builder.setMessage(contents);
	                         builder.setTitle("该项资产已做盘盈登记\n" );
	                         //builder.setIcon(R.drawable.icon);

	                         builder.setPositiveButton("确定", new android.content.DialogInterface.OnClickListener()
	                         {
	                             @Override
	                             public void onClick(DialogInterface dialog, int which)
	                             {
	                           		
	                             }
	                         });
	                       
	                         builder.create().show();
	                    	return;
	                    }
	                    
	                    
	                    mEditText_asset_number.setText(Number);
	                    mEditText_asset_barcode.setText(Barcode);
	                    mEditText_asset_name.setText(name);
	                    mEditText_asset_playnum.setText(playnum);
	                    mEditText_asset_carcode.setText(Carcode);
	                    mEditText_asset_spec.setText(spec);
	                    
	                    assetSave.setClickable(true);
	    	            if(flag.equals("0")) spinner.setSelection(0,true);
	    	            else if(flag.equals("1")) spinner.setSelection(1,true);
	    	            else if(flag.equals("2")) spinner.setSelection(2,true);
	    	            
	    	            
	    	            assetSave.setClickable(true);
	    	            assetSave.setBackground(getResources().getDrawable(R.drawable.bp_btn_bg));
	    	           // if(Flag.equals("1"))   mTextView_pan_count.setText("已盘数："+Qty);
	    	            
	    	           // mEditText_count.requestFocus();
	    	           // mEditText_count.setSelection(mEditText_count.getText().length());
	    	           
	            }
	            else
	            {
	    			Toast.makeText(
					getApplicationContext(),
					"数据不存在！数据指针为空",
					Toast.LENGTH_SHORT).show();
	            	
	            }

	            cursor.close();
	            assetServices.closeDB();
	            
	           

	        } catch (Exception e)
	        {
	            // TODO Auto-generated catch block
	        	Toast.makeText(
						getApplicationContext(),
						"数据不存在！",
						Toast.LENGTH_SHORT).show();
	            e.printStackTrace();
	        }
		//--------------------------------------

        
       }

	
	public void onSaveClick(View view){
		
		if(bool_assetAdd==false){
		String barNum = mEditText_asset_barcode.getText().toString();

		if(!TextUtils.isEmpty(barNum))
		{
			AssetServices AssetServices = new AssetServices(getApplication());
			 boolean updateOk = AssetServices.update(spinnerNum, barNum);
			 if(updateOk) {
				 
				 Toast.makeText(
							getApplicationContext(),
							"保存数据成功！",
							Toast.LENGTH_SHORT).show();
	
			 }
			 else{
				 Toast.makeText(
							getApplicationContext(),
							"保存失败！",
							Toast.LENGTH_SHORT).show();
			 }
		}
		else{
			Toast.makeText(
					getApplicationContext(),
					"保存失败，数据为空！",
					Toast.LENGTH_SHORT).show();
		}
		
		}
		else if(bool_assetAdd==true)//资产盘盈
		{
			String assetnumber = mEditText_asset_number.getText().toString();
			String assetBarcodeScan = mEditText_asset_barcode_scan.getText().toString();
			if(!TextUtils.isEmpty(assetnumber) && !TextUtils.isEmpty(assetBarcodeScan)){
			
				AssetServices addAssetServices = new AssetServices(getApplication());
				boolean add = addAssetServices.addAsset(assetnumber, assetBarcodeScan);
				if(add == true)
				{
					addAssetServices.update(3, assetBarcodeScan);
					Toast.makeText(
							getApplicationContext(),
							"保存成功！",
							Toast.LENGTH_SHORT).show();
				}
			}
		}
		
	}
	
	
	/**
	 * 
	 * 盘赢处理
	 */
	public void assetAdd()
	{
		
		
		  AlertDialog.Builder builder = new Builder(LookAsset.this);

        //builder.setMessage(contents);
        builder.setTitle("你扫描的资产条码不存在，是否做盘盈登记？\n" );
        //builder.setIcon(R.drawable.icon);

        builder.setPositiveButton("是", new android.content.DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
          		//GoodsServices goodsService = new GoodsServices(getApplication());
          		//goodsService.reset();
            	bool_assetAdd = true;
            	mEditText_asset_number.setBackground(getResources().getDrawable(R.drawable.btnbg2));
            	mEditText_asset_number.setEnabled(true);
            	mEditText_asset_number.requestFocus();
            	
            	assetSave.setClickable(true);
	            assetSave.setBackground(getResources().getDrawable(R.drawable.bp_btn_bg));
	            spinner.setClickable(false);
            
            }
        });
        builder.setNegativeButton("否", new android.content.DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {

            }
        });
        builder.create().show();
        
        
	}
	
	
	/**
	 *
	 * @param view
	 * 
	 * 商品盘点
	 */
	public void onBackClick(View view){  		  

       this.finish();
      
      }


	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		
	    Toast.makeText(this, "Button clicked!", Toast.LENGTH_SHORT).show(); 
//		if (event.getAction() == MotionEvent.ACTION_UP) {
//			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//			boolean isOpen = imm.isActive();// isOpen若返回true，则表示软键盘输入法打开
//			if (isOpen) {
//			
//				
//				imm.hideSoftInputFromWindow(
//						mEditText_scan.getWindowToken(), 0);
//				imm.hideSoftInputFromWindow(
//						mEditText_linecode.getWindowToken(), 0);
//				imm.hideSoftInputFromWindow(
//						mEditText_encode.getWindowToken(), 0);
//				
//			}
			
			

//		}
		
		return false;
	}



	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}  
	
	
}
