﻿package com.android.util;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import android.os.Environment;
import android.os.StatFs;

public class FileUtils {
	private String SDCardRoot;

	public FileUtils() {
		//得到当前外部存储设备的目录
		SDCardRoot = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator;
	}
	/**
	 * 在SD卡上创建文件
	 * 
	 * @throws IOException
	 */
	public File createFileInSDCard(String fileName,String dir) throws IOException {
		File file = new File(SDCardRoot+ dir + File.separator + fileName);
		//System.out.println("file---->" + file);
		file.createNewFile();
		return file;
	}
	
	/**
	 * 在SD卡上创建目录
	 * 
	 * @param dirName
	 */
	public File creatSDDir(String dir) {
		File dirFile = new File(SDCardRoot + dir + File.separator);
		//System.out.println(dirFile.mkdirs());
		return dirFile;
	}

	/**
	 * 判断SD卡上的文件夹是否存在
	 */
	public boolean isFileExist(String fileName,String path){
		File file = new File(SDCardRoot + path + File.separator + fileName);
		return file.exists();
	}
	
	/**
	 * 将一个InputStream里面的数据写入到SD卡中
	 */
	public File write2SDFromInput(String path,String fileName,InputStream input){
		
		File file = null;
		OutputStream output = null;
		try{
			creatSDDir(path);
			file = createFileInSDCard(fileName, path);
			output = new FileOutputStream(file);
			byte buffer [] = new byte[4 * 1024];//申请10K空间
			int temp ;
			while((temp = input.read(buffer)) != -1){
				output.write(buffer,0,temp);
				Thread.sleep(100);//延时等待写SD操作结束
			}
			output.flush();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			try{
				output.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
		return file;
	}
	
	/**
	 * 检查SD卡是否存在
	 * @return
	 */
	 public static boolean ExistSDCard() {
	            if (android.os.Environment.getExternalStorageState().equals(
	              android.os.Environment.MEDIA_MOUNTED)) {
	             return true;
	            } else
	             return false;
	           }
	 
	 /**
	  * SD卡剩余空间
	  * @return
	  */
	 public static long getSDFreeSize(){
	      //取得SD卡文件路径
	      File path = Environment.getExternalStorageDirectory(); 
	      StatFs sf = new StatFs(path.getPath()); 
	      //获取单个数据块的大小(Byte)
	      long blockSize = sf.getBlockSize(); 
	      //空闲的数据块的数量
	      long freeBlocks = sf.getAvailableBlocks();
	      //返回SD卡空闲大小
	      //return freeBlocks * blockSize;  //单位Byte
	      //return (freeBlocks * blockSize)/1024;   //单位KB
	      return (freeBlocks * blockSize)/1024 /1024; //单位MB
	    } 
	 
	 /**
	  * 
	  * SD卡总容量
	  * @return
	  */
	 public long getSDAllSize(){
	      //取得SD卡文件路径
	      File path = Environment.getExternalStorageDirectory(); 
	      StatFs sf = new StatFs(path.getPath()); 
	      //获取单个数据块的大小(Byte)
	      long blockSize = sf.getBlockSize(); 
	      //获取所有数据块数
	      long allBlocks = sf.getBlockCount();
	      //返回SD卡大小
	      //return allBlocks * blockSize; //单位Byte
	      //return (allBlocks * blockSize)/1024; //单位KB
	      return (allBlocks * blockSize)/1024/1024; //单位MB
	    }     
	
	
		public static String convertStreamToString(InputStream is) throws Exception {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line).append("\n");
			}
			reader.close();
			return sb.toString();
		}

		
		public static String getStringFromFile(String filePath) throws Exception {
			File fl = new File(filePath);
			FileInputStream fin = new FileInputStream(fl);
			String ret = convertStreamToString(fin);
			fin.close();
			return ret;
		}

}